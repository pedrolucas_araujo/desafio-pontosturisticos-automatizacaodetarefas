const { src, dest, watch } = require("gulp");
const sass = require("gulp-sass")(require("sass"));

function styles() {
    return src("src/styles/utils/main.scss")
        .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
        .pipe(dest("dist"));
}

function sentinel() {
    watch("src/styles/*.scss", { ignoreInitial: false }, styles);
}
exports.sentinel = sentinel;
